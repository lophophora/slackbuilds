#!/bin/sh
# $Id: lua.SlackBuild,v 1.9 2018/04/22 09:52:36 root Exp root $
# Copyright 2009, 2011, 2013, 2015, 2018  Eric Hameleers, Eindhoven, NL
# All rights reserved.
#
#   Permission to use, copy, modify, and distribute this software for
#   any purpose with or without fee is hereby granted, provided that
#   the above copyright notice and this permission notice appear in all
#   copies.
#
#   THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
#   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#   MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
#   IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
#   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
#   USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
#   OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
#   OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
#   SUCH DAMAGE.
# -----------------------------------------------------------------------------
#
# Slackware SlackBuild script 
# ===========================
# By:          Eric Hameleers <alien@slackware.com>
# For:         lua
# Descr:       a powerful, fast, lightweight, embeddable scripting language
# URL:         http://www.lua.org
# Needs:       
# Changelog:   
# 5.1.4-1:     28/Jul/2009 by Eric Hameleers, Eindhoven, NL
#              * Initial build.
# 5.1.4-2:     26/Aug/2011 by Eric Hameleers <alien@slackware.com>
#              * Rebuilt for Slackware 13.37 using an official patch which
#                squashes several bugs.
# 5.1.5-1:     15/oct/2013 by Eric Hameleers, Eindhoven, NL
#              * Update. Note that Lua 5.2 is incompatible with most programs
#                that use it.
# 5.1.5-2:     12/oct/2015 by Eric Hameleers <alien@slackware.com>
#              * Instead of a patch and several lines of sed, use the patch-less
#                logic from the slackbuilds.org script; adds missing liblua.so
#                Thanks to the SBo script authors:
#                - Written by Menno Duursma
#                - Modified by the SlackBuilds.org project
#                - Modified by Aaron W. Hsu
#                - Updated by Matteo Bernardini
# 5.1.5-3:     22/apr/2018 by Eric Hameleers <alien@slackware.com>
#              * Rebuilt for -current after boost upgrade.
# 
# Run 'sh lua.SlackBuild' to build a Slackware package.
# The package (.tgz) and .txt file as well as build logs are created in /tmp .
# Install it using 'installpkg'. 
#
# -----------------------------------------------------------------------------


PRGNAM=lua
VERSION=${VERSION:-5.3.5}
BUILD=${BUILD:-3}
NUMJOBS=${NUMJOBS:-" -j7 "}
TAG=${TAG:-snowball}

DOCS="COPYRIGHT HISTORY INSTALL README \
      doc/*.html doc/*.css doc/logo.gif doc/cover.png"

# Where do we look for sources?
SRCDIR=$(cd $(dirname $0); pwd)

# Place to build (TMP) package (PKG) and output (OUTPUT) the program:
TMP=${TMP:-/tmp/build}
PKG=$TMP/package-$PRGNAM
OUTPUT=${OUTPUT:-/tmp}

SOURCE[0]="$SRCDIR/${PRGNAM}-${VERSION}.tar.gz"
SRCURL[0]="http://www.lua.org/ftp/${PRGNAM}-${VERSION}.tar.gz"

##
## --- with a little luck, you won't have to edit below this point --- ##
##

# Automatically determine the architecture we're building on:
if [ -z "$ARCH" ]; then
  case "$(uname -m)" in
    i?86) ARCH=i586 ;;
    arm*) readelf /usr/bin/file -A | egrep -q "Tag_CPU.*[4,5]" && ARCH=arm || ARCH=armv7hl ;;
    # Unless $ARCH is already set, use uname -m for all other archs:
    *) ARCH=$(uname -m) ;;
  esac
  export ARCH
fi

case "$ARCH" in
  i?86)      SLKCFLAGS="-O2 -march=${ARCH} -mtune=i686"
             SLKLDFLAGS=""; LIBDIRSUFFIX=""
             ;;
  x86_64)    SLKCFLAGS="-O2 -fPIC"
             SLKLDFLAGS="-L/usr/lib64"; LIBDIRSUFFIX="64"
             ;;
  armv7hl)   SLKCFLAGS="-O2 -march=armv7-a -mfpu=vfpv3-d16"
             SLKLDFLAGS=""; LIBDIRSUFFIX=""
             ;;
  armv6hl)   SLKCFLAGS="-O2 -march=armv6 -mfpu=vfp -mfloat-abi=hard"
             SLKLDFLAGS=""; LIBDIRSUFFIX=""
             ;;
  *)         SLKCFLAGS="-O2"
             SLKLDFLAGS=""; LIBDIRSUFFIX=""
             ;;
esac

# Exit the script on errors:
set -e
trap 'echo "$0 FAILED at line ${LINENO}" | tee $OUTPUT/error-${PRGNAM}.log' ERR
# Catch unitialized variables:
set -u
P1=${1:-1}

# Save old umask and set to 0022:
_UMASK_=$(umask)
umask 0022

# Create working directories:
mkdir -p $OUTPUT          # place for the package to be saved
mkdir -p $TMP/tmp-$PRGNAM # location to build the source
mkdir -p $PKG             # place for the package to be built
rm -rf $PKG/*             # always erase old package's contents
rm -rf $TMP/tmp-$PRGNAM/* # remove the remnants of previous build
rm -rf $OUTPUT/{configure,make,install,error,makepkg}-$PRGNAM.log
                          # remove old log files

# Source file availability:
for (( i = 0; i < ${#SOURCE[*]}; i++ )) ; do
  if ! [ -f ${SOURCE[$i]} ]; then
    echo "Source '$(basename ${SOURCE[$i]})' not available yet..."
    # Check if the $SRCDIR is writable at all - if not, download to $OUTPUT
    [ -w "$SRCDIR" ] || SOURCE[$i]="$OUTPUT/$(basename ${SOURCE[$i]})"
    if [ -f ${SOURCE[$i]} ]; then echo "Ah, found it!"; continue; fi
    if ! [ "x${SRCURL[$i]}" == "x" ]; then
      echo "Will download file to $(dirname $SOURCE[$i])"
      wget -nv -T 20 -O "${SOURCE[$i]}" "${SRCURL[$i]}" || true
      if [ $? -ne 0 -o ! -s "${SOURCE[$i]}" ]; then
        echo "Fail to download '$(basename ${SOURCE[$i]})'. Aborting the build."
        mv -f "${SOURCE[$i]}" "${SOURCE[$i]}".FAIL
        exit 1
      fi
    else
      echo "File '$(basename ${SOURCE[$i]})' not available. Aborting the build."
      exit 1
    fi
  fi
done

if [ "$P1" == "--download" ]; then
  echo "Download complete."
  exit 0
fi

# --- PACKAGE BUILDING ---

echo "++"
echo "|| $PRGNAM-$VERSION"
echo "++"

cd $TMP/tmp-$PRGNAM
echo "Extracting the source archive(s) for $PRGNAM..."
tar -xvf ${SOURCE[0]}
cd ${PRGNAM}-${VERSION}
chown -R root:root .
chmod -R u+w,go+r-w,a+X-s .

echo Building ...

# Compile and install:
# Fix up a to-be-installed header and the pkgconfig file
sed -i "s|/usr/local|/usr|" src/luaconf.h
sed -i "s|lib/lua|lib$LIBDIRSUFFIX/lua|" src/luaconf.h
#sed -i "s|/usr/local|/usr|" etc/lua.pc
#sed -i "s|prefix}/lib|prefix}/lib${LIBDIRSUFFIX}|g" etc/lua.pc

make linux \
  CFLAGS="$SLKCFLAGS \$(MYCFLAGS)" \
  INSTALL_TOP=/usr \
  INSTALL_LIB=/usr/lib${LIBDIRSUFFIX} \
  INSTALL_LMOD=/usr/share/lua/5.3 \
  INSTALL_CMOD=/usr/lib${LIBDIRSUFFIX}/lua/5.3

make linux install \
  CFLAGS="$SLKCFLAGS \$(MYCFLAGS)" \
  INSTALL_TOP=$PKG/usr \
  INSTALL_LIB=$PKG/usr/lib${LIBDIRSUFFIX} \
  INSTALL_LMOD=$PKG/usr/share/lua/5.3 \
  INSTALL_CMOD=$PKG/usr/lib${LIBDIRSUFFIX}/lua/5.3

# Now let's build the shared library
mkdir -p shared
cd shared
  ar -x $PKG/usr/lib${LIBDIRSUFFIX}/liblua.a
  gcc -ldl -lreadline -lhistory -lncurses -lm -shared *.o -o liblua.so.$VERSION
  cp -a liblua.so.$VERSION $PKG/usr/lib${LIBDIRSUFFIX}
  ( cd $PKG/usr/lib${LIBDIRSUFFIX}
    ln -s liblua.so.$VERSION liblua.so.5.3
    ln -s liblua.so.$VERSION liblua.so.5
    ln -s liblua.so.$VERSION liblua.so
  )
cd ..

# Install a pkg-config file:
#install -D -m 0644 etc/lua.pc $PKG/usr/lib${LIBDIRSUFFIX}/pkgconfig/lua.pc

# Add documentation:
mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION
cp -a $DOCS $PKG/usr/doc/$PRGNAM-$VERSION || true
cat $SRCDIR/$(basename $0) > $PKG/usr/doc/$PRGNAM-$VERSION/$PRGNAM.SlackBuild
chown -R root:root $PKG/usr/doc/$PRGNAM-$VERSION
find $PKG/usr/doc -type f -exec chmod 644 {} \;

# Compress the man page(s):
if [ -d $PKG/usr/man ]; then
  find $PKG/usr/man -type f -name "*.?" -exec gzip -9f {} \;
  for i in $(find $PKG/usr/man -type l -name "*.?") ; do ln -s $( readlink $i ).gz $i.gz ; rm $i ; done
fi

# Strip binaries (if any):
find $PKG | xargs file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true

# Add a package description:
mkdir -p $PKG/install
cat $SRCDIR/slack-desc > $PKG/install/slack-desc

# Build the package:
cd $PKG
makepkg --linkadd y --chown n $OUTPUT/${PRGNAM}-${VERSION}-${ARCH}-${BUILD}${TAG}.${PKGTYPE:-tgz} 2>&1 | tee $OUTPUT/makepkg-${PRGNAM}.log
cd $OUTPUT
md5sum ${PRGNAM}-${VERSION}-${ARCH}-${BUILD}${TAG}.${PKGTYPE:-tgz} > ${PRGNAM}-${VERSION}-${ARCH}-${BUILD}${TAG}.${PKGTYPE:-tgz}.md5
cd -
cat $PKG/install/slack-desc | grep "^${PRGNAM}" > $OUTPUT/${PRGNAM}-${VERSION}-${ARCH}-${BUILD}${TAG}.txt

# Restore the original umask:
umask ${_UMASK_}

