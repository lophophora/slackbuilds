# slackbuilds
A collection of slackware build scripts

## Contributing 

Find existing slackbuilds with
```bash
find $SEARCHDIR -name *.SlackBuild -exec cp --parents {} $SLACKBUILDS/ \;
```
Add new slackbuilds with

```bash
git add $SLACKBUILDS/*.SlackBuild
```
Upload with
```
git commit -m "Message"
git push
```

